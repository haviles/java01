package deloitte.academy.lesson01.comparisons;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase con metodos comparativos (==,!=,>,<,>=,<=) para usar con numeros
 * enteros (var1,var2)
 * 
 * @author haviles
 *
 */
public class Comparisons {
	private static final Logger LOGGER = Logger.getLogger(Comparisons.class.getName());

	/**
	 * Funcion para saber si dos numeros enteros son iguales (var1==var2)
	 * 
	 * @param var1 numero entero 1
	 * @param var2 numero entero 2
	 * @return Retorna True si son iguales, False si no lo son
	 */
	public boolean igualNumerosEnteros(int var1, int var2) {
		boolean bandera = true;
		try {
			if (var1 == var2) {
				LOGGER.info(var1 + " == " + var2 + ": " + true);
				bandera = true;
			} else {
				LOGGER.info(var1 + " == " + var2 + ": " + false);
				bandera = false;
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception Occur", e);
		}
		return bandera;
	}

	/**
	 * Funcion para saber si dos numeros flotantes son iguales (var1 == var2)
	 * 
	 * @param var1 numero flotante a comparar
	 * @param var2 numero floante a comparar
	 * @return Retorna True si son iguales, False si no lo son
	 */
	public boolean igualNumerosFloat(float var1, float var2) {
		boolean bandera = true;
		try {
			if (var1 == var2) {
				LOGGER.info("Resultado de comparar " + var1 + " y " + var2 + ": " + true);
				bandera = true;
			} else {
				LOGGER.info("Resultado de comparar " + var1 + " y " + var2 + ": " + false);
				bandera = false;
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception Occur", e);
		}
		return bandera;
	}

	/**
	 * Funcion para comparar 2 Strings (var1==var2)
	 * 
	 * @param var1 String a comparar
	 * @param var2 String2 a comparar
	 * @return Retorna True si son iguales, False si no lo son
	 */
	public boolean igualString(String var1, String var2) {
		boolean bandera = true;
		try {
			if (var1 == var2) {
				LOGGER.info("Resultado de comparar " + var1 + " y " + var2 + ": " + true);
				bandera = true;
			} else {
				LOGGER.info("Resultado de comparar " + var1 + " y " + var2 + ": " + false);
				bandera = false;
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception Occur", e);
		}
		return bandera;
	}

	/**
	 * Funcion para saber si dos chars son iguales (var1==var2)
	 * 
	 * @param var1 Char a comparar
	 * @param var2 Char2 a comparar
	 * @return Retorna True si son iguales, False si no lo son
	 */
	public boolean igualChar(char var1, char var2) {
		boolean bandera = true;
		try {
			if (var1 == var2) {
				LOGGER.info("Resultado de comparar " + var1 + " y " + var2 + ": " + true);
				bandera = true;
			} else {
				LOGGER.info("Resultado de comparar " + var1 + " y " + var2 + ": " + false);
				bandera = false;
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception Occur", e);
		}
		return bandera;
	}

	/**
	 * Funcion para saber si dos numeros enteros son Iguales (var1 != var2)
	 * 
	 * @param var1 Numero entero a comparar
	 * @param var2 Numero entero a comparar con el primero
	 * @return Retorna True si son Iguales, False si no lo son
	 */
	public boolean diferenteNumerosEntero(int var1, int var2) {
		boolean bandera = true;
		try {
			if (var1 != var2) {
				LOGGER.info(var1 + " != " + var2 + ": " + true);
				bandera = true;
			} else {
				LOGGER.info(var1 + " != " + var2 + ": " + false);
				bandera = false;
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception Occur", e);
		}
		return bandera;
	}

	/**
	 * Funcion para saber si el primer numero dado es menor al segundo (var1<var2)
	 * 
	 * @param var1 Numero entero Inicial a comparar
	 * @param var2 Numero entero a comparar con el primero
	 * @return Retorna true si var1 es menor a var2, false si no
	 */
	public boolean menorAnumerosEntero(int var1, int var2) {
		boolean bandera = true;
		try {
			if (var1 < var2) {
				LOGGER.info(var1 + " < " + var2 + ": " + true);
				bandera = true;
			} else {
				LOGGER.info(var1 + " < " + var2 + ": " + false);
				bandera = false;
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception Occur", e);
		}
		return bandera;
	}

	/**
	 * Funcion para saber si el primer numero dado es mayor al segundo (var1>var2)
	 * 
	 * @param var1 Numero Inicial a comparar
	 * @param var2 Numero a comparar con el primero
	 * @return Retorna true si var1 es menor a var2, false si no
	 */
	public boolean mayorAnumerosEntero(int var1, int var2) {
		boolean bandera = true;
		try {
			if (var1 > var2) {
				LOGGER.info(var1 + " > " + var2 + ": " + true);
				bandera = true;
			} else {
				LOGGER.info(var1 + " > " + var2 + ": " + false);
				bandera = false;
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception Occur", e);
		}
		return bandera;
	}

	/**
	 * Funcion para saber si el primer numero es mayor o igual al segundo
	 * (var1>=var2)
	 * 
	 * @param var1 Numero Inicial a comparar
	 * @param var2 Numero a comparar con el primero
	 * @return Retorna true si var1 es menor a var2, false si no
	 */
	public boolean mayorOigualNumerosEntero(int var1, int var2) {
		boolean bandera = true;
		try {
			if (var1 >= var2) {
				LOGGER.info(var1 + " >= " + var2 + ": " + true);
				bandera = true;
			} else {
				LOGGER.info(var1 + " >= " + var2 + ": " + false);
				bandera = false;
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception Occur", e);
		}
		return bandera;
	}

	/**
	 * Funcion para saber si el primer numero es menor o igual al segundo
	 * (var1<=var2)
	 * 
	 * @param var1 Numero entero Inicial a comparar
	 * @param var2 Numero entero a comparar con el primero
	 * @return Retorna true si var1 es menor a var2, false si no
	 */
	public boolean menorOigualNumerosEntero(int var1, int var2) {
		boolean bandera = true;
		try {
			if (var1 <= var2) {
				LOGGER.info(var1 + " <=  " + var2 + ": " + true);
				bandera = true;
			} else {
				LOGGER.info(var1 + " <= " + var2 + ": " + false);
				bandera = false;
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception Occur", e);
		}
		return bandera;
	}

}
