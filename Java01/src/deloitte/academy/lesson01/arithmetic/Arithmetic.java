package deloitte.academy.lesson01.arithmetic;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase con metodos Aritmeticos, contiene los metodos para sumar, restar,
 * dividir, y calcular el modulo de 2 numeros enteros dependiendo la funcion.
 * 
 * @author haviles
 *
 */
public class Arithmetic {
	private static final Logger LOGGER = Logger.getLogger(Arithmetic.class.getName());

	/**
	 * Funcion para sumar todos los valores enteros, positivos de un arreglo
	 * {var1,var2,...varN}
	 * 
	 * @param arregloNum Arreglo de Enteros Positivos para sumar
	 * @return Suma de todos los valores del arreglo
	 */
	public int sumaEnteros(int[] arregloNum) {
		int suma = 0;
		try {
			for (int var : arregloNum) {
				suma = var + suma;
			}
			LOGGER.info("Resultado de la Suma: " + suma);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception Occur", e);
		}
		return suma;
	}

	/**
	 * Funcion para realizar una resta de dos numeros (num1 -num2)
	 * 
	 * @param num1 Numero al que se le restara otro numero
	 * @param num2 Numero a restar
	 * @return Numero Entero num1-num2
	 */
	public int restaEnteros(int num1, int num2) {
		int resta = 0;
		try {
			resta = num1 - num2;
			LOGGER.info("Resultado de la Resta: " + resta);
			// return resta;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception Occur", e);
		}
		return resta;
	}

	/**
	 * Funcion para realizar una multiplicacion de 2 numeros (num1 *num2)
	 * 
	 * @param num1 Numero 1 a multiplicar
	 * @param num2 Numero 2 a multiplicar por el primero
	 * @return Retorna Resultado Entero de la Multiplacion
	 */
	public int multiplicacionEnteros(int num1, int num2) {
		int mult = 0;
		try {
			mult = num1 * num2;
			LOGGER.info("Resultado de la Multiplicacion: " + mult);

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception Occur", e);
		}
		return mult;
	}

	/**
	 * Funcion para realizar la division de 2 numeros (num1/num2)
	 * 
	 * @param num1 Numero Divisor
	 * @param num2 Numero Divido
	 * @return numero Flotante
	 */
	public float divisionNumeros(float num1, float num2) {
		float div = 0;
		try {
			div = num1 / num2;
			LOGGER.info("Resultado de la Division: " + div);

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception Occur", e);
		}
		return div;
	}

	/**
	 * Funcion para calcular el modulo de 2 numeros (num1%num2)
	 * 
	 * @param num1 Numero 1 positivo
	 * @param num2 Numero 2 positivo
	 * @return Numero Flotante resultado del modulo
	 */
	public float moduloNumeros(float num1, float num2) {
		float modulo = 0;
		try {
			modulo = num1 % num2;
			LOGGER.info("Resultado del modulo: " + modulo);

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception Occur", e);
		}
		return modulo;
	}
}
