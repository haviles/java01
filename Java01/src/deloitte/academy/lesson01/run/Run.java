package deloitte.academy.lesson01.run;

import deloitte.academy.lesson01.arithmetic.Arithmetic;
import deloitte.academy.lesson01.comparisons.Comparisons;
import deloitte.academy.lesson01.logicalOperators.LogicalOperators;

public class Run {
	/**
	 * Clase para instanciar la clase Arithmetic, LogicalOperators, Comparisons y
	 * probar todos sus metodos
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// INSTANCIACION DE OBJETOS
		Arithmetic arith = new Arithmetic();
		Comparisons comparisons = new Comparisons();
		LogicalOperators logicalOperators = new LogicalOperators();

		System.out.println("======================Objeto Arithmetic========================");
		// PRUEBA DE METODOS DEL OBJETO DE ARITHMETIC
		// Prueba SUMA
		int numeros[] = { 45, 87, 28 };
		arith.sumaEnteros(numeros);

		// Prueba RESTA
		arith.restaEnteros(28, 42);

		// Prueba DIVISION
		arith.divisionNumeros(222, 65);

		// Prueba MODULO
		arith.moduloNumeros(7, 20);

		// PRUEBA DE METODOS DE OBJETO COMPARISON
		System.out.println("======================Objeto Comparisons========================");

		// Prueba IGUAL A
		comparisons.igualNumerosEnteros(12, 12);

		// Prueba DIFERENTE A
		comparisons.diferenteNumerosEntero(3, 2);

		// Prueba MENOR A
		comparisons.menorAnumerosEntero(5, 3);

		// Prueba MAYOR A
		comparisons.mayorAnumerosEntero(19, 27);

		// Prueba MAYOR O IGUAL A
		comparisons.mayorOigualNumerosEntero(5, 12);

		// Prueba MENOR O IGUAL A
		comparisons.menorOigualNumerosEntero(6, 6);

		// PRUEBA DE METODOS DE OBJETO Logical Operators
		System.out.println("======================Objeto LogicalOperators========================");

		// PRUEBA AND &&
		logicalOperators.mayorA10yMenorA20(17);

		// PRUEBA OR ||
		logicalOperators.menorA10oMayorA20(23);

		// PRUEBA XOR ^
		logicalOperators.validarXor(true, false);

		// PRUEBA NOT
		logicalOperators.validarNot(false);
	}

}
