package deloitte.academy.lesson01.logicalOperators;

import java.util.logging.Logger;

/**
 * Clase con metodos Logicos Operadores aplicados, AND,XOR,NOT,OR
 * 
 * @author haviles
 *
 */
public class LogicalOperators {
	private static final Logger LOGGER = Logger.getLogger(LogicalOperators.class.getName());

	/**
	 * Funcion para saber si un numero es mayor a 10 y menor a 20
	 * 
	 * @param var Numero a validar
	 * @return Retorna true si es mayor a 10 y menor a 20, False si no lo es
	 */
	public boolean mayorA10yMenorA20(int var) {
		if (var > 10 && var < 20) {
			LOGGER.info(var + " > 10 && " + var + " < 20: true");
			return true;
		} else {
			LOGGER.info(var + " > 10 && " + var + " < 20: false");
			return false;
		}
	}

	/**
	 * Funcion para validar si un numero es menor a 10 o mayor a 20
	 * 
	 * @param var Numero a validar
	 * @return Retorna True si es menor a 10 o si es mayor a 20, False si no lo es
	 */
	public boolean menorA10oMayorA20(int var) {
		if (var < 10 || var > 20) {
			LOGGER.info(var + " < 10 || " + var + " > 20: true");
			return true;
		} else {
			LOGGER.info(var + " < 10 || " + var + " > 20: false");
			return false;
		}
	}

	/**
	 * Funcion para validar XOR
	 * 
	 * @param var1 Booleano 1
	 * @param var2 Booleano 2
	 * @return True si el XOR es verdadero, False si es falso
	 */
	public boolean validarXor(boolean var1, boolean var2) {
		if (var1 ^ var2) {
			LOGGER.info(var1 + " xor " + var2 + ": true");
			return true;
		} else {
			LOGGER.info(var1 + " xor " + var2 + ": false");
			return false;
		}
	}

	/**
	 * Funcion para validar NOT
	 * 
	 * @param var1 Booleano 1
	 * @param var2 Booleano 2
	 * @return
	 */
	public boolean validarNot(boolean var) {
		if (!var) {
			LOGGER.info(var + " is false");
			return true;
		} else {
			LOGGER.info(var + " is true");
			return false;
		}
	}
}
